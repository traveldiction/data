
# provider : cette directive définit le fournisseur avec lequel terraform va interagir. Il s’agit ici d’aws.
# profile : nous précisons que dans les différents profils possibles de notre configuration aws locale, nous allons utiliser default.
# region : le nom de la région par défaut



provider "aws" {
   profile    = "default"
   region     = "eu-west-1"
 # region = "eu-west-1"
 }
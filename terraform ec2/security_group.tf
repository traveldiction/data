# vpc_id : définit à quel vpc ce security group appartient.
# ingress : va définir les flux en entrée vers les ressources qui se rattachent à ce security group, dans le cadre de cet article notre instance.
# from_port : pour le port source, ici le 22 pour autoriser SSH.
# to_port : pour le port destination, ici le 22 pour autoriser SSH.
# protocol : pour le type du protocole du flux.
# cidr_blocks : indique une liste d’adresses acceptées en entrée. Il s’agit ici de l’adresse de mon bureau.
# egress : va définir les flux sortant des ressources attachées à notre security group.
# from_port : pour le port source. Ici 0 car nos allons tout autoriser.
# to_port : pour le port destination. Ici 0 car nos allons tout autoriser.
# protocol : pour le type du protocol. Ici un cas spécial avec une valeur à -1 car nous autorisons tous les types de flux en sortie.
# cidr_blocks : pour les adresses de destination. Ici 0.0.0.0/0 pour tout autoriser.






resource "aws_default_security_group" "sg_ssh" {
     vpc_id      = "${aws_default_vpc.default.id}"
 ingress {
     # TLS (change to whatever ports you need)
     from_port   = 22
     to_port     = 22
     protocol    = "tcp"
     # Please restrict your ingress to only necessary IPs and ports.
     # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
     cidr_blocks     = ["1.2.3.4/32"]
   }
 egress {
     from_port       = 0
     to_port         = 0
     protocol        = "-1"
     cidr_blocks     = ["0.0.0.0/0"]
   }
 }
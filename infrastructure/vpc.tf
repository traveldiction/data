# aws_default_vpc indique une ressource de type aws_default_vpc, un cas spécial de vpc qui est le vpc par défaut associé à votre compte AWS.
# Terraform peut le modifier mais pas le supprimer.
# tags est uniquement l’association d’une étiquette contenant du texte apportant de l’information sur notre ressource.
# Nous créons ici une étiquette nommée Name à laquelle nous associons le texte Default VPC. 


resource "aws_default_vpc" "default" {
   tags = {
     Name = "Default VPC"
   }
 }
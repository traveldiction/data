resource "aws_elasticsearch_domain" "travel_diction" {
  domain_name           = "travel-diction"
  elasticsearch_version = "2.3"
  

  cluster_config  {
    dedicated_master_enabled = "true"
    instance_count           = "4"
    instance_type = "t2.micro.elasticsearch"
    zone_awareness_enabled   = "true"
  }
  ebs_options {
        ebs_enabled = "true"
        volume_size = "25"
  }
}  




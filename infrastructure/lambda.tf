
locals{
    lambda_zip_location = "output/script-test-lambda.zip" 
}


data "archive_file" "script-test-lambda" {
  type        = "zip"
  source_file = "script-test-lambda.py"
  output_path = "${local.lambda_zip_location}"
}



resource "aws_lambda_function" "test_lambda" {
  filename      = "${local.lambda_zip_location}"
  function_name = "main"
  role          = "${aws_iam_role.lambda_role.arn}"
  handler       = "script-test-lambda.main"

#source_code_hash = "${filebase64sha256("lambda_function_payload.zip")}"

  runtime = "python3.7"


}
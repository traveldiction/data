resource "aws_s3_bucket" "my-data-project" {
  bucket        = "data-236154145284776567"
  acl           = "public-read"
  
}

resource "aws_s3_bucket_object" "keys1" {
  bucket = aws_s3_bucket.my-data-project.id
  key    = "keys1.zip"
  acl    = "public-read"
  source = "./keys1.zip"
}

resource "aws_s3_bucket_object" "keys2" {
  bucket = aws_s3_bucket.my-data-project.id
  key    = "keys2.zip"
  acl    = "public-read"
  source = "./keys2.zip"
}
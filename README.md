Pour le fichier v-finalscrap.ypnb
 ===>
 
 #############
 def ecriture(data): 
    path = "**globalpenfriends.xlsx**" # ==> Nom du fichier ou on écrit
    lecture = pd.read_excel(path,"Profils",usecols="B:T")
    lecture.dropna(inplace=True,thresh=3)

    wb = xw.Book(path)
    PROFILS = wb.sheets[**'Profils'**]  # ==> Nom de **la feuille** du fichier ou l'on ecrit
    ...
    

#############
token = "https://www.globalpenfriends.com/penpals/"

id_url = 10
id=id_url - 1
pages = get_pages(token,id_url,300)  ===> Crée une liste de page https allant de l'id 10 à a 309
                                        ==> 300 représente la taille de la liste

pages = ["https://www.globalpenfriends.com/penpals/10", "https://www.globalpenfriends.com/penpals/11", "https://www.globalpenfriends.com/penpals/309"  ]

###########

payload = {
                    'action': 'login',
                    'login': **'robotfou09@yopmail.com'**,    ===> Nom du compte globalpenfriends
                    'pswd': **"azerty123"**                 ===> Mot de passe du compte globalpenfirends
                 }
proxy = "88.199.21.76:80"   ==> adresse IP --> Une adresse par compte 
                           
###########

**LES SORTIES AFFICHAGEs POSSIBLEs **

La premiére ligne qui s'affiche est :
        ==>  **"connexion ip réussite**  --> Connexion réussite
        
        ==> **"connexion ip echec"** --> changez l'adresse ip et réexécuter la boucle

Durant la boucle, les sorties possibles sont :

        ==> ("itération :",iteration)                   |\
        ==> "Temps d execution : %s secondes ---" %     |---> Nombre de page scrapper + durée exécution + ecriture des data dans le fichier excel
        
        ==> "succés : id "  --> La page numero id a été scrapper    
        
        ==> "Skipping. Connnection error","proxy :"  --> La page numéro id n'existe pas
                                                     \--> SI + de 10 id se suivent alors le compte ou l'ip a été bloqué, stopper le programme et changer de compte et ip
        
        ==> "erreur 201"  --> le Profils à buguer ou a été desactivé
